\begin{tikzpicture}
  \node[draw] (ide) {IDE};
  \node[circle,draw,left=8em of ide] (mnemo) {Mnemo};
  \node[circle,draw,right=8em of ide] (copilot) {Copilot};
  \node[above=of ide,ellipse,draw] (developer) {Developer};
  \draw[<->] (developer) to (ide);

  \draw[->] (ide) to node[auto,above] {Comments} (copilot);
  \draw[->,bend left] (copilot) to node[auto,below] {Code} (ide);

  \draw[->] (ide) to node[auto,above] {Argot} (mnemo);
  \draw[->,bend right] (mnemo) to node[auto,below] {Argot} (ide);

  %% Mnemosyne Muses
  \node[above right=4em and 1em of mnemo] (m1) {Muse};
  \node[above left=6em and 0em of mnemo] (m2) {Muse};
  \node[above=2em of mnemo] (m3) {Muse};
  \node[above right=6em and 2em of mnemo] (m4) {Muse};
  \node[above left=4em and 1em of mnemo] (m5) {Muse};

  \draw[dotted] (m1) to (m2);
  \draw[dotted] (m5) to (m4);

  \draw (m1) to (mnemo);
  \draw (m2) to (mnemo);
  \draw (m3) to (mnemo);
  \draw (m4) to (mnemo);
  \draw (m5) to (mnemo);

  \node[fit=(m1) (m2) (m3) (m4) (m5)] (muses) {};

  \draw[decorate,thick,decoration={brace,amplitude=1em}] (muses.north west) -- (muses.north east)
       node[midway,yshift=2.5em] {CS Research};

  %% Copilot Infrastructure
  \node[above=2.5em of copilot, starburst, draw] (gpt3) {GPT3};
  \node[above=1.5em of gpt3, cloud, draw] (internet) {Internet};

  \draw[->] (internet) to node[auto,right] {Training} (gpt3);
  \draw[<->] (copilot) to node[auto,right] {Inference} (gpt3);

\end{tikzpicture}
