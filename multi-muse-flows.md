---
layout: default.liquid
title: Multi-Muse Flows
flows:
  - tests-to-core-to-code:
    name: "Trinity &rarr; Refactoring &rarr; TypeWriter &rarr; Hypothesis"
    graphic: 2
    muses: "test-view,trinity,refactor,type-writer,hypothesis-tests"
    desc: The Test View Muse collects input/output examples from a Python program.  These examples are then passed to the Trinity muse which synthesizes an implementation in an appropriate DSL.  The implementation flows back to the Argot Core muse which converts the DSL to Python for insertion into the program.  The refactoring muse then wraps this Python snippet into a function.  The TypeWriter muse synthesizes a type for this new function.  The Type View muse inserts the type into the file. This type then flows to the Hypothesis muse which inserts a new test into the file and performs property-based testing of the function.
    video: /video/through-demo-1.mp4
  - types-to-tests-to-synth:
    name: "TypeWriter &rarr; Hypothesis &rarr; GenPatcher"
    graphic: 1
    muses: "type-view,hypothesis-tests,genpatcher"
    desc: The TypeWriter Muse collects types for a function in a Python program.  These types are then used by the Hypothesis property based testing tool to generate test inputs for the function.  The GenPatcher automated program repair tool is then invoked using these tests to evolve a version of the function which is able to successfully execute against all valid test inputs.
  - test-to-genpatcher-with-refactoring:
    name: "Autocomplete &harr; GenPatcher &harr; Refactorings"
    graphic: 3
    muses: "autocomplete,refactor,genpatcher"
    desc: GenPatcher is invoked because of a failing test for a Python function.  GenPatcher is then able to leverage other Muses while it searches for a repair.  In effect GenPatcher operates much like a developer guiding a team of Muses towards a new version of the source code.  In particular GenPatcher might make use of the Refactorings mutations to take large structured steps through the repair space modifying disparate source code, and might make use of the autocomplete Muse to take large unstructured steps through repair space modifying localized source code.
  - autocomplete-to-hypothesis:
    name: "Function Generator &rarr; Hypothesis"
    muses: "autocomplete,hypothesis"
    desc: The function generator autocomplete is invoked, populating a function body using [OpenAI's Codex model](https://openai.com/blog/openai-codex/).  Upon acceptance of the suggested completion, hypothesis is invoked to insert a new property-based test for the function, protecting against weaknesses which may be present in the model result.
    video: /video/function-generator-hypothesis-demo-1.mp4
---

Multi-Muse Flows
================

Many anticipated use cases will flow through multiple Muses as
illustrated below.

{% for flow in flows %}

{% if flow.graphic %}

<div onmouseover="show_flow({{ flow.graphic }});muses('{{ flow.muses }}')" onmouseout="show_flow(0);muses('');">

{% else %}

<div>

{% endif %}

{{  flow.name }}
:   {{ flow.desc }}</br>{% if flow.video %}&rarr; Use case [video]({{ flow.video | url }}).{% endif %}

</div>

{% endfor %}

<div class="figure">
<p>
<img class="flow" id="flow-0" src='{{ "/flow.svg" | url }}'>
<img class="flow" id="flow-1" style="display:none;" src='{{ "/flow-1.svg" | url }}'>
<img class="flow" id="flow-2" style="display:none;" src='{{ "/flow-2.svg" | url }}'>
<img class="flow" id="flow-3" style="display:none;" src='{{ "/flow-3.svg" | url }}'>
</p>
</div>
<script>
function show_flow(num){
  Array.from(document.getElementsByClassName("flow")).forEach(function(img){
    if (img.id == "flow-"+num){
      img.style.display = 'inline';
    } else {
      img.style.display = 'none';
    }
  })
};
function muses(muses){
  var muses = muses.split(",");
  console.log(muses);
  Array.from(document.getElementsByClassName("muse")).forEach(function(desc){
    if (muses.includes(desc.id)){
      console.log(desc.id + " HIGHLIGHT")
      desc.style.background = 'yellow';
    } else {
      console.log(desc.id)
      desc.style.background = 'white';
    }
  })
};
</script>
