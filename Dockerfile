FROM gone/eleventy
RUN npm install -g markdown-it
RUN npm install -g markdown-it-anchor
RUN npm install -g markdown-it-table-of-contents

CMD sh