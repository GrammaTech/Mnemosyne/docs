---
layout: default.liquid
title: Architecture
---

# Tests LSP Extension

[[TOC]]

This document describes a series of endpoints for interacting with Argot to
handle test cases.

## Shared Definitions

```typescript
export namespace TestCaseKind {
  export const UnitTest = 1;
  export const BinaryTest = 2;
  ...
}

interface UnitTestCaseResult {
    expected: string | number | boolean | ...;
}

interface BinaryTestCaseResult {
    stdout?: string;
    stderr?: string;
    exitCode: number;
}

interface TestCase {
    name: string | path | ...;
    target: string | path | Location | ...;
    testCaseKind: TestCaseKind;
    arguments: List<string | number | ...>;
    expected: UnitTestCaseResult | BinaryTestCaseResult | ...;
}
```

## Test Case Addition

**Client Capability**
- propert name (optional): `argotTests.testCaseAdd`
- propertyType: `ArgotTestCaseAddClientCapabilities`

```typescript
interface ArgotTestCaseAddClientCapabilities {
    /**
     * Whether completion supports dynamic registration.
     */
    dynamicAdd?: boolean;

    /**
     * The client supports the following `ArgotTestCaseAdd`
     * capabilities.
     */
   ...
}
```

**Server Capability**
- property name (optional): `argotTests/testCaseAddProvider`
- property type: `boolean | TestCaseAddOptions` where
  `TestCaseAddOptions` is defined as:

```typescript
interface testCaseAddOptions {
    /**
     * Test kinds this server may register.
     */
    testCaseAddKinds?: TestCaseKind[];
}
```

**Registration Options** *(TBD)*

```typescript
interface TestCaseAddRegistrationOptions
    extends TestDocumentRegistrationOptions, TestCaseExecutionOptions {
  ...
}
```

**Request**
- method: `argotTests/testCaseAdd`
- params: `TestCaseAddParams`

```typescript
interface TestCaseAddParams {
    testCase: TestCase;
}
```

**Response**
- result: `TestCaseAddResponse`

```typescript
interface TestCaseAddResponse {
    didRegister: boolean;
    testCaseAddID: number | undefined;
}
```

## Test Case Lookup

**Server Capability**
- property name (optional): `argotTests/testCaseLookupProvider`
- property type: `boolean | TestCaseLookupOptions` where
  `TestCaseLookupOptions` is defined as:

```typescript
interface testCaseLookupOptions {
    /**
     * Test kinds this server may register.
     */
    testCaseLookupKinds?: TestCaseKind[];
}
```

**Registration Options** *(TBD)*

```typescript
interface TestCaseLookupRegistrationOptions
    extends TestDocumentRegistrationOptions, TestCaseExecutionOptions {
  ...
}
```

**Request**
- method: `argotTests/testCaseLookup`
- params: `TestCaseLookupParams`

```typescript
interface TestCaseLookupParams {
    /**
     * Testidentifier is either the test name (string) or test case identifier
     * (integer) as registered with the server.
     */
    testCaseIdentifier: string | number | ...;
}
```

**Response**
- result: `boolean | TestCaseLookupResponse`

```typescript
interface TestCaseLookupResponse {
    successful: boolean;
    testCase?: TestCase;
}
```

## Test Suite Lookup

*It is possible we would like to support multiple test suites. In that case,
 test case registration should also take a suite ID.*

**Server Capability**
- property name (optional): `argotTests/testSuiteLookupProvider`
- property type: `boolean` indicating if the server supports this.

**Registration Options** *(TBD)*

```typescript
interface TestSuiteLookupRegistrationOptions
    extends TestDocumentRegistrationOptions, TestSuiteExecutionOptions {
  ...
}
```

**Request**
- method: `argotTests/testSuiteLookup`

Returns all registered tests.

**Response**
- result: `boolean | TestSuiteLookupResponse`

```typescript
interface TestSuiteLookupResponse {
    testCases: TestCase[];
}
```

## Test Case Check

**Client Capability**
- propert name (optional): `argotTests.testCaseCheck`
- propertyType: `ArgotTestCaseCheckClientCapabilities`

```typescript
interface ArgotTestCaseCheckClientCapabilities {
    /**
     * Whether completion supports dynamic registration.
     */
    dynamicRegistration?: boolean;

    /**
     * The client supports the following
     * `ArgotTestCaseCheckClientCapabilities` capabilities.
     */
   ...
}
```

**Server Capability**
- property name (optional): `argotTests/testCaseCheckProvider`
- property type: `boolean | TestCaseCheckOptions` where
  `TestCaseCheckOptions` is defined as:

```typescript
interface TestCaseCheckOptions {
    /**
     * Test kinds this server may run.
     */
    testCaseCheckKinds?: TestCaseKind[];
}
```

**Registration Options** *(TBD)*

```typescript
interface TestCaseCheckRegistrationOptions
    extends TestDocumentRegistrationOptions, TestCaseExecutionOptions {
}
```

**Request**
- method: `argotTests/testCaseCheck`
- params: `TestCaseCheckParams` defined as follows:

```typescript
interface TestCaseCheckParams {
    /**
     * Testidentifier is either the test name (string) or test case identifier
     * (integer) as registered with the server.
     */
    testCaseIdentifier: string | number | ...;
    context?: TestCaseContext;
}
```

**Response**
- result: `TestCaseCheckResult | ProgressReport | null`:

```typescript
interface TestCaseCheckResult {
    testCaseID: int;
    output?: UnitTestResult | BinaryTestResult | ...;
    successful: boolean;
}
```
