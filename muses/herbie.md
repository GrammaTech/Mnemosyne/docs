---
tags: [muse, front-muse]
id: herbie
name: Herbie
brief: improve floating-point code stability and accuracy
slug: herbie
stat: integrated
url: https://herbie.uwplse.org
desc: Integration of the [Herbie](https://herbie.uwplse.org) system for improving the accuracy of floating-point expressions.
rationale: Directly useful to developers, and for improving the product of other synthesis techniques.
video: /video/herbie.mp4
---
