---
tags: muse
id: refactor
name: Refactor
desc: An automated refactoring engine which implements common refactorings over multiple programming languages.
rationale: Provides a useful feature for developers automating a mundane task.  Provides a useful primitive to other muses like GenPatcher.
stat: integrated
---
