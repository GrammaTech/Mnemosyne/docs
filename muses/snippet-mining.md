---
title: Snippet Mining
tags: [muse, front-muse]
id: snippet-mining
name: Snippet Mining
brief: example API usage snippets
url: https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/snippet-mining
desc: Implementation of the algorithm described in "Exempla Gratis (E.G.) - Code Examples for Free" to provide developers with example API usage snippets mined from a corpus of programs
rationale: Provides developers with idiomatic usage patterns of APIs as a supplement to documentation and code search tools
stat: integrated
video: /video/snippet-mining-demo.mp4
script: snippet-mining
---

Snippet Mining
============

First you will need an environment with the `snippet-mining-server`
script in which to launch the Argot Server.  You may either install
the `snippet-mining-server` script yourself locally as described in
[Muses/snippet-mining][] or launch Argot Server in its provided
Docker image.

``` bash
docker pull mnemo/argot-server
```

With Mnemosyne running and the snippet-mining muse loaded, you can now
invoke the muse for API usage examples derived from mining a large corpus
of programs.  In VSCode, or another editor with support for snippet
completions, open Python source file, and add the following code:

``` python
import json
j
```

In VSCode, a completion action should be triggered after typing the `j`
and example snippets for the usage of various JSON library APIs shown.
You may select an example from the list; those parts of the API usage not
common to all examples in the corpus from which the snippet was derived
will be highlighted for you to populate.  You may press tab to cycle thru
these parts of the snippet you may wish to update.

Please note that your editor must support snippets for the snippet-mining
server to work properly.  Additionally, at certain locations in the code
where it is unlikely a snippet is required (e.g. when importing a library),
snippet suggestions will not be shown.

[Muses/snippet-mining]: https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/snippet-mining
