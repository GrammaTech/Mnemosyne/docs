---
layout: default.liquid
title: Galois Autocomplete
tags: [muse, front-muse]
id: galois-autocomplete
name: Galois Autocomplete
brief: machine learning based code completion
slug: autocomplete
url: https://github.com/galois-autocompleter/galois-autocompleter
desc: Fork of the [Galois project](https://github.com/galois-autocompleter/galois-autocompleter) for machine learning based code completion.
rationale: Generically useful across any domain both for developers and also as a primitive operation available to other muses working to synthesize code.
stat: integrated
video: /video/galois-autocomplete-demo.mp4
script: galois-autocomplete
---

Galois Autocomplete
============

First you will need an environment with the `autocomplete` script
in which to launch the Argot Server.  You may either install the
`autocomplete` script yourself locally as described in [Muses/autocomplete][]
or launch Argot Server in its provided Docker image.  The muse itself
is a fork of the [Galois Autocompleter][] project with changes such as
the addition of an LSP server frontend.

``` bash
docker pull mnemo/argot-server
```

With Mnemosyne running and the Galois autocomplete muse loaded, you can now
invoke the muse for code completions guided by machine learning.  Open up a
Python source file, and add the following code:

``` python
if __name__ ==
```

Place the cursor after the `==` and then invoke a code completion request.  (E.g.,
in Emacs with `M-x completion-at-point` or in VSCode by pressing `Ctrl+Space`).
The LSP server will return a list of applicable completions, at the top of which
should be `'__main__'` and `"__main__"`.

[Muses/autocomplete]: https://gitlab.com/GrammaTech/Mnemosyne/muses/autocomplete
[Galois Autocompleter]: https://github.com/galois-autocompleter/galois-autocompleter
