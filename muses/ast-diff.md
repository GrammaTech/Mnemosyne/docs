---
tags: muse
id: ast-diff
name: MergeResolver
url: https://mergeresolver.github.io
desc: Automatically merge conflicting branches of software projects leveraging AST-level differencing and maintaining test-suite functionality.  This technology is currently also deployed as [the MergeResolver GitHub App](https://mergeresolver.github.io).
rationale: Will be important when we start having enough concurrent muse activity that the argot-server has to resolve conflicts between muses.
stat: prototyped
---
