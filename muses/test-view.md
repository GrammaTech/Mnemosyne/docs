---
tags: muse
id: test-view
name: Test View
desc: Collects and replaces tests from program source.
rationale: Essential for serializing test information to/from program source freeing other muses from having to worry about the details of serialization to multiple source languages and libraries.
stat: prototyped
---
