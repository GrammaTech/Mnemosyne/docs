---
tags: [muse, front-muse]
id: function-generator
name: Function Generator
brief: Function body generation
desc: Creation of a function body based on the prototype and documentation string using [OpenAI's Codex model](https://openai.com/blog/openai-codex/).
rationale: Generically useful for developers and other muses
stat: prototyped
video: /video/function-generator-demo.mp4
url: https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/function-generator
---
