---
tags: [muse, front-muse]
id: dig
name: DIG
brief: trace-based invariant generation technique
slug: dig
stat: integrated
url: https://github.com/unsat/dig
desc: Execution trace driven Dynamic Invariant Generation (DIG).
rationale: Invariants are inputs to many downstream program synthesis and validation muses.  They are also directly useful to developers as (programmatically checkable) documentation of intent.
video: /video/dig.mp4
---
