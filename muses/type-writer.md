---
tags: muse
id: type-writer
name: TypeWriter
url: https://gitlab.com/GrammaTech/Mnemosyne/muses/type-writer
slug: type-writer
desc: Integration of the [TypeWriter](https://arxiv.org/pdf/1912.03768.pdf) technique for the inference of types in Python.
rationale: Type inference enables many other muses such as synthesis and test generation.
stat: integrated
video: /video/typewriter-demo.mp4
---
