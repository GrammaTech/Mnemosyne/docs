---
tags: [muse, front-muse]
id: genpatcher
name: GenPatcher
brief: automated program repair
desc: Automated program repair technique.
rationale: This muse has the greatest potential to leverage the other muses serving as a proxy for the developer themself.
stat: prototyped
video: /video/genpatcher-demo-01.mp4
---
