---
tags: muse
id: godbolt
name: Compiler Explorer
slug: godbolt
stat: integrated
url: https://godbolt.org
desc: Integration of the [Compiler Explorer](https://github.com/compiler-explorer/compiler-explorer) interactive compiler framework.
rationale: Directly useful to developers, and useful both as direct input to other synthesis techniques, and for evaluating candidates from synthesis techniques.
video: /video/godbolt.mp4
---
