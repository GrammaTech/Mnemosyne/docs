---
tags: [muse, front-muse]
id: hypothesis-tests
name: Hypothesis Tests
brief: type-driven automated testing
slug: hypothesis-tests
desc: Type-driven automated tests using the hypothesis library
rationale: Tests help developers to express requirements across any domain.  They are also an enabler of downstream synthesis muses.
stat: integrated
video: /video/hypothesis-tests-demo.mp4
script: hypothesis-tests
layout: default.liquid
title: Hypothesis Tests
---

Hypothesis Tests
============

First you will need an environment with the `hypothesis-tests` script
in which to launch the Argot Server.  You may either install the
`hypothesis-tests` script yourself locally as described in
[Muses/Hypothesis-Tests][] or launch Argot Server in its provided Docker
image.

``` bash
docker pull mnemo/argot-server
```

Additionally, on the client machine you will want to have the `hypothesis[all]`
package installed from `pip3` if you wish to use the tests locally.

With Mnemosyne running with the hypothesis tests muse loaded, you can now invoke
the muse to synthesize automatted tests using the hypothesis python library
and type annotations.  Open up a Python source file, and add the following
function:

``` python
from typing import List

def replace_ones(l: List[int], v: int) -> List[int]:
    '''
    replace all elements that are 1 with v
    '''
    for i in range(len(l)):
        if l[i] == 1:
            l[i] = v
    return l
```

Highlight the `replace_ones` function and then invoke a code action.  (E.g., in
Emacs with `M-x eglot-code-actions` or in VSCode by simply highlighting the
expressions.)

An appropriate hypothesis test will be synthesized by the muse and offered for
selection by client.  After selecting the code action, the relevent code
will be inserted.  If a falsifying example for the newly created test is
found by the hypothesis library, an @example decorator will be added to the
test as well.

[Muses/Hypothesis-Tests]: https://gitlab.com/GrammaTech/Mnemosyne/muses/hypothesis-tests
