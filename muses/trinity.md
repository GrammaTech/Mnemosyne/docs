---
tags: [muse, front-muse]
id: trinity
name: Trinity
brief: extensible systhesis framework for data science
url: https://github.com/fredfeng/Trinity
slug: trinity
desc: Integration of the [Trinity An Extensible Synthesis Framework for Data Science](https://sites.cs.ucsb.edu/~yanju/publications/vldb19_trinity.pdf) technique for program synthesis.
stat: integrated
video: /video/trinity-demo.mp4
script: trinity
layout: default.liquid
title: Trinity Muse
---

Trinity Muse
============

First you will need an environment with the `trinity-muse` executable
in which to launch the Argot Server.  You can either build
`trinity-muse` yourself locally as described in [Muses/Trinity][] or
launch Argot Server in its provided Docker image.

``` bash
docker pull mnemo/argot-server
```

With Mnemosyne running with the Trinity muse loaded you can now invoke
trinity to synthesize mathematical expressions from provided
input/output pairs.  Open up a JavaScript or a Python source file and
comment off a series of expressions like the following.

``` python
# 4, 3 -> 3
# 6, 3 -> 9
# 1, 2 -> -2
# 1, 1 -> 0
```

or

``` js
// 4, 3 -> 3
// 6, 3 -> 9
// 1, 2 -> -2
// 1, 1 -> 0
```

Highlight the expressions and then invoke a code action.  (E.g., in
Emacs with `M-x eglot-code-actions` or in VSCode by simply
highlighting the expressions.)

An appropriate mathematical expression will be synthesized by trinity,
it will then be translated from the trinity mathematical DSL into
Python or JavaScript, and finally it will be inserted back into the
source code file.

[Muses/Trinity]: https://gitlab.com/GrammaTech/Mnemosyne/muses/trinity
