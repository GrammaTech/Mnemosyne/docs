---
tags: muse
id: regel
name: REGEL
url: https://github.com/utopia-group/regel
slug: regel
desc: Integration of the [Multi-modal Synthesis of Regular Expressions](https://arxiv.org/abs/1908.03316) technique for the synthesis of regular expressions from natural language and examples.
rationale: Regular expressions are very widely useful and hard to write.  Their synthesis both automates a common developer activity and encourages the developer to focus on the inputs to REGEL -- namely documentation and examples.
stat: prototyped
---
