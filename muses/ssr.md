---
tags: [muse, front-muse]
id: ssr
name: SSR
desc: AST-level software search and replacement.  Enables automated structured library replacement using pre-defined rules.
brief: AST-level software search and replace
rationale: Provides a useful feature for developers automating a mundane task.  Eventual library replacement will ensure more source code stays ahead of N-day vulnerabilities.
stat: integrated
video: /video/ssr-muse-demo.mp4
---
