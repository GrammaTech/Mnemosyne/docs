---
layout: default.liquid
title: Argument Predictor
tags: [muse, front-muse]
id: argument-predictor
name: Argument Predictor
brief: probabilistic completion of callsite arguments
url: https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor
desc: Probabilistic code completion of callsite arguments using a statistical database.
rationale: Argument names and constants are non-random and correlated with corresponding parameter names and common usage patterns.  Their prediction for the client removes the risk of argument mismatch errors and assists in code development.
stat: integrated
video: /video/argument-predictor-demo.mp4
script: argument-predictor
---

Argument Predictor
============

First you will need an environment with the `argument_predictor` script
in which to launch the Argot Server.  You may either install the
`argument_predictor` script yourself locally as described in [Muses/code-predictor][]
or launch Argot Server in its provided Docker image.

``` bash
docker pull mnemo/argot-server
```

With Mnemosyne running and the argument predictor muse loaded, you can now
invoke the muse for callarg name and constant predictions guided by a statistical
database of callsite information.  Open up a Python source file, and add the following
code:

``` python
import os
MY_DIR = os.path.dirname(
```

Place the cursor after the `(` and then invoke a code completion request.  (E.g.,
in Emacs with `M-x completion-at-point` or in VSCode by pressing `Ctrl+Space`).
The LSP server will return a list of applicable completions, in this case
`__file__`, as the special variable is in scope and commonly used as the first
parameter to `os.path.dirname`.

[Muses/code-predictor]: https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor
