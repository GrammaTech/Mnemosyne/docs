---
tags: muse
id: lambda-net
name: LambdaNet
url: https://arxiv.org/abs/2005.02161
slug: lambda-net
desc: Integration of the [LambdaNet](https://arxiv.org/abs/2005.02161) technique for the inference of types in JavaScript.
rationale: Type inference enables many other muses such as synthesis and test generation.
stat: integrated
---
