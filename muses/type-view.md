---
tags: muse
id: type-view
name: Type View
desc: Collects and replaces types from program source.
rationale: Essential for serializing type information to/from program source freeing other muses from having to worry about the details of serialization to multiple source languages and libraries.
stat: prototyped
---
