---
layout: default.liquid
title: Usage
---

Usage
=====

[[TOC]]


## Collect Mnemosyne Logs
Run Mnemosyne from the Docker image collecting logging output:
```bash
docker run --rm -p 10003:10003 mnemo/argot-server \
    |tee ~/.argot-server/argot-$(date +%Y-%m-%d-%H-%M-%S).log
```

## GPU Acceleration
<a id=gpu></a>

If your machine includes a Nvidia GPU, the GPU may be utilized within
the Docker image for acceleration of certain muses.  To begin, install
the `nvidia-container-toolkit` as follows:

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt update
sudo apt-get install nvidia-container-toolkit
```

## Install and Run Mnemosyne Locally

### Set up Argot Server for development

The easy way to develop against Argot Server is to take advantage of the Docker image.

1. Pull the Docker image:

    ```bash
    docker pull mnemo/argot-server
    ```

2. Clone Argot Server and enter the directory:

    ```bash
    git clone https://gitlab.com/GrammaTech/Mnemosyne/argot-server.git
    cd argot-server
    ```

3. Some Muses run as separate images. To run those, you will need to
   install [docker-compose][].

   After installing docker-compose, in the Argot Server directory, run:
   ``` bash
   make dev-up
   ```

   The `dev-up` target differs from the `up` target in (1) running the
   muses on the host network, and (2) disabling the `mnemo` service so
   you can run your own.

   If you have a GPU, set `GPU` so the muses can use the GPU:

   ``` bash
   env GPU=1 make dev-up
   ```

3. Run Docker with the Argot Server directory bind-mounted:

    ```bash
    docker run --network=host --rm -it -v $(pwd):/root/quicklisp/local-projects/argot-server mnemo/argot-server rlwrap sbcl
    ```

5. (Optional) From the SBCL prompt, run [Sly][] or [SLIME][] in the
   normal way.

6. Evaluate the following Lisp code in your REPL to start the server:

   ``` lisp
   (ql:quickload '(:argot-server))
   (defparameter *argot-server* (argot-server:launch-server :network "host"))
   ```

   This starts the server with all the muses currently available.
   Muses are enabled conditionally based on what is present in the
   environment.

   After this command, Argot Server will be running in the background,
   and `*argot-server*` will be set to a handle you can use to stop
   the server with `argot-server:server-handle-stop`.

### Editor Setup

Install an LSP client for your editor. Most text editors and IDEs
support [LSP][] and Mnemosyne should work with most full featured LSP
clients.

### VS Code Walkthrough

Support for VS Code is provided through a Mnemosyne VS Code extension.

1. Download the .vsix file for the latest release of the Mnemosyne
   extension from its [releases page][vsix-releases].

2. Install the .vsix file in [the usual way][install-vsix].

3. Once the Argot Server is running, call `Mnemosyne: Connect to Argot
   Server` from the Command Palette.

4. You can now request an LSP CodeAction. Open a new file, insert the
   following, and save it with a `.js` extension:

    ``` js
    // For JS
    const x = 2 + 2
    x + y
    ```

   Select the contents of the file. You should see a lightbulb icon
   appear to indicate code actions are available. You can click on the
   lightbulb, or press `Ctrl+.`, to bring up a drop-down of code
   actions. Select `Inline Variable` and you should see:

    ``` js
    // For JS
    const x = 2 + 2
    2 + 2 + y
    ```

#### Emacs Walkthrough

1. Emacs users can use [Eglot][]. (If you already use [lsp-mode][], it
   should work, but is not tested.)

   Eglot requires at least Emacs 26.1. Users of older Ubuntu
   distributions can easily upgrade using a [PPA][].

   Eglot is included in the ELPA package repository, so assuming a
   clean Emacs, installation is as simple as `M-x package-install
   eglot`. Existing Emacs users may want to upgrade their installed
   packages first.

   Once you have Eglot installed, add
   [`eglot-argot.el`][eglot-argot.el] to your load path so you can use
   [Argot][] extensions.

2. From an Emacs buffer, in a Lisp, Python, or JavaScript project,
   connect to Mnemosyne with `M-x eglot`. Start working in this buffer
   and you'll see Eglot providing contextual information and
   completions. (You can see the raw LSP traffic with `M-x
   eglot-events-buffer`.)

3. You can now request a generic LSP CodeAction.  Place something like
   the following in an Emacs buffer (NOTE: this is a simple example to
   start):

    ``` lisp
    ;; For Lisp
    (let ((x (+ 2 2)))
      (+ y x))
    ```

    ``` js
    // For JS
    const x = 2 + 2
    x + y
    ```

    Then mark a region in the buffer containing the above form and no
    other forms and call `M-x eglot-code-actions`. This will send the
    region to the Argot Server which will return a list of applicable
    refactorings to Emacs. Hit `TAB` to view all possible
    refactorings. Try `Inline Variable` and you should see:

    ``` lisp
    ;; For Lisp
    (let ((x (+ 2 2)))
      (+ y (+ 2 2)))
    ```

    ``` js
    // For JS
    const x = 2 + 2
    2 + 2 + y
    ```

### Vim Walkthrough

The easiest way to get running with Vim is to use [coc.nvim][], which
notwithstanding its name supports Vim as well as Neovim.

1. Install the [Plug][] package manager. You will need `curl` and
   `git`.

   ``` bash
   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
   ```

2. In your `~/.vim/vimrc` or `~/.config/nvim/init.vim`, set up COC and
   coc-argot:

   ``` clike
   call plug#begin('~/.vim/plugged')

   Plug 'neoclide/coc.nvim', {'branch': 'release'}
   Plug 'https://gitlab.com/GrammaTech/Mnemosyne/coc-argot', {'do': 'npm install && npm run compile'}

   call plug#end()
   ```

3. In the shell, install the client by running:

    ``` bash
    vim +PlugInstall +qall
    nvim --headless +PlugInstall +qall
    ```

    Or you can run `:PlugInstall` from inside Vim or Nvim.

4. Restart Vim and find a JavaScript file. Connect to Mnemosyne with
   `:CocCommand mnemosyne.connect`.

5. You can now request a generic LSP CodeAction. Place something like
   the following in an buffer:

    ``` js
    const x = 2 + 2
    x + y
    ```

    Then use visual mode to highlight the above form and type `:CocAction`.
    This will send the region to the Argot Server which will return a list of
    applicable refactorings. Try `Inline Variable` and you should see:

    ``` js
    const x = 2 + 2
    2 + 2 + y
    ```

### Mnemosyne-relevant LSP Commands and Extensions

For the moment, in Vim and Emacs, you have to explicitly ask for help.
You can do this with an LSP [Code Action Request](CodeAction). In Emacs with Eglot you trigger a code action by running `M-x eglot-code-action`. In Vim you trigger code actions with `:CocAction`.

[pyls]: https://github.com/palantir/python-language-server
[javascript-typescript-langserver]: https://github.com/sourcegraph/javascript-typescript-langserver
[Eglot]: https://github.com/joaotavora/eglot
[kelleyk]: https://launchpad.net/~kelleyk/+archive/ubuntu/emacs
[CodeAction]: https://microsoft.github.io/language-server-protocol/specification#textDocument_codeAction
[LSP]: https://en.wikipedia.org/wiki/Language_Server_Protocol
[LanguageClient-neovim]: https://github.com/autozimu/LanguageClient-neovim
[Plug]: https://github.com/junegunn/vim-plug
[vsix-releases]: https://gitlab.com/GrammaTech/Mnemosyne/mnemosyne-vscode/-/releases
[install-vsix]: https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix
[docker-compose]: https://docs.docker.com/compose/install/
[eglot-argot.el]: https://gitlab.com/GrammaTech/Mnemosyne/eglot-argot/-/blob/master/eglot-argot.el
[coc.nvim]: https://github.com/neoclide/coc.nvim
[Argot]: https://gitlab.com/GrammaTech/Mnemosyne/argot
[Sly]: https://github.com/joaotavora/sly
[SLIME]: https://common-lisp.net/project/slime/
