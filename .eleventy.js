const config = {}

module.exports = function(eleventyConfig) {
  // Customized Markdown Export.
  let markdownIt = require("markdown-it");
  let md = markdownIt({html: true})
  md.use(require("markdown-it-deflist"));
  md.use(require("markdown-it-anchor"));
  let options = {"includeLevel": [2,3]};
  md.use(require("markdown-it-table-of-contents"), options);
  eleventyConfig.setLibrary("md", md);

  // It will just copy extensions (like "css") that it doesn't
  // normally recognize.
  eleventyConfig.setTemplateFormats(["md", "svg", "css", "js", "mp4", "vtt", "png", "jpg"]);

  // Internal Path
  eleventyConfig.addShortcode('pathPrefix', function() {
    return config.pathPrefix;
  })

  // Syntax Highlighting
  eleventyConfig.addPlugin(require("@11ty/eleventy-plugin-syntaxhighlight"));

  // Icon
  eleventyConfig.addPassthroughCopy("favicon.ico");

  // Filter for adding abbrev to acronyms.
  glossary = {
    "AFRL": "Air Force Research Laboratory",
    "AST": "Abstract Syntax Tree",
    "CST": "Concrete Syntax Tree",
    "DARPA": "Defense Advanced Research Projects Agency",
    "IDE": "Integrated Development Enviornment",
    "KLOC": "Thousand Lines of Code",
    "LOC": "Lines of Code",
    "LSP": "Language Server Protocol",
    "REPL": "Read Eval Print Loop",
    "SBSE": "Search Based Software Engineering"
  }
  eleventyConfig.addFilter("glossary", function(text) {
    for (const key in glossary) {
      text = text.replace(new RegExp("\\b" + key + "\\b", 'g'),
                         `<abbr title="${glossary[key]}">${key}</abbr>`);
      text = text.replace(new RegExp("\\b" + key + "s\\b", 'g'),
                         `<abbr title="${glossary[key]}">${key}</abbr>s`);
    }
    return text;
  });

  return config;
};
