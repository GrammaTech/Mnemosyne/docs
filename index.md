---
layout: default.liquid
title: Home
---

<!-- https://www.greeklegendsandmyths.com/mnemosyne.html -->
<!-- img/mnemosyne.png -->

<figure style="padding-top:3em;" class="w3-right my-half">
    <img class=w3-card
         style="filter: saturate(50%);"
         src="{{ "/img/apollo-and-muses.jpg"|url }}">
    <figcaption><small><p>(Apollo dancing with the Muses)</br>
    Mnemosyne provides automated software development collaborators or "muses."</p></small>
    </figcaption>
</figure>

Mnemosyne
=========

> Mnemosyne: Titan goddess of memory and remembrance, inventor of language, mother of the muses.

<center>
<span style="text-decoration:underline">
<i class="fa fa-rss"></i> Read about Mnemosyne's <a href="{{ "/mnemosyne-and-github-copilot"|url }}">relationship to GitHub's CoPilot.</a>
</span>
</center>

<div style="clear:both;"></div>

Mnemosyne is your *automated software development assistant*.  It
integrates with your IDE and works with you suggesting code, types,
and tests as you work; and continually checking the consistency of
your documentation, types, tests, and implementation&mdash;whether
that means running test suites or type checkers, or using ML
techniques to ensure your documentation matches your implementation.
Mnemosyne applies statistical machine learning, formal methods, and
Search Based Software Engineering (SBSE) to enable developers to write
higher quality, higher assurance, and more standards compliant code
more quickly.  Mnemosyne transcends CI/CD by making live substantive
contributions to the software development process as the developer
works.  Mnemosyne speaks [LSP][], meaning it integrates seamlessly
with most popular text editors and IDEs.  Mnemosyne is extensible,
easily collaborating with varied program synthesis modules or
"[Muses]({{ "/muses" | url }})."

<div class="w3-container w3-margin-bottom">
<center>
<div style="max-width: 1000px;">
<video id="player" playsinline controls muted>
    <source src="{{"video/integrated-demo-june.mp4"|url}}#t=0.01" type="video/mp4">
    <!-- <track kind="captions" label="English captions" src="{{"video/integrated-demo-june.vtt"|url}}" srclang="en" default /> -->
</video>
</div>
</center>
</div>

<div class="w3-row-padding">

<div class="w3-half w3-margin-bottom">
<div class="w3-container w3-card w3-light-gray intro-card">

## QuickStart

See [Usage]({{ "/usage" | url }}) for detailed usage information.

1. Install [docker-compose][].

2. Clone the Argot Server repository.
    ```bash
    git clone https://gitlab.com/GrammaTech/Mnemosyne/argot-server.git
    ```

3. In the argot-server directory, start Argot Server.
   ```bash
   make up
   ```

   If you need to run on the host network (e.g. you are using a
   restrictive VPN), use `host-up` instead.
   ```bash
   make host-up
   ```

   If you have a GPU, and Docker is [configured to work with it]({{
   "/usage" | url }}#gpu), you can pass `GPU=1`:

   ```bash
   GPU=1 make up
   ```

4. Open a C, C++, Python, JavaScript, or Common Lisp file and connect
   your editor or IDE to the server.

    From Visual Studio Code:

      1. Install the latest [Mnemosyne VS Code extension][vsix]. ([How
         to install a `.vsix` file][vsix-howto].)
      2. Open Command Palette (Control+Shift+P) and connect to the
         Argot Server with `Mnemosyne: Connect to Argot Server`.
      3. Click on the code action “bulb” icon or request code actions
         with `Control+.`

    From Emacs:

      1. Install [Eglot][] `M-x package-install-package eglot RET`.
      2. Add [`eglot-argot.el`][eglot-argot.el] to your load path and
         `(require ‘eglot-argot)`.
      3. Connect to the Argot Server with `M-x eglot` using
         `localhost:10003` for the host and port.
      4. Invoke Mnemosyne with `M-x eglot-code-actions`.

    From Vim:

      1. Install `coc.nvim` and `coc-argot` with [Plug][]:
         ```
         Plug 'neoclide/coc.nvim', {'branch': 'release'}
         Plug 'https://gitlab.com/GrammaTech/Mnemosyne/coc-argot',
         \ {'do': 'npm install && npm run compile'}
         ```
      2. Connect to the Argot Server with `CocCommand mnemosyne.connect`.
      3. Use `:CocAction` to request Mnemosyne code actions.

</div>
</div> <!-- end quick-start -->

<div class="w3-half w3-margin-bottom">
<div class="w3-container w3-card w3-light-gray intro-card">

## Architecture

See [System Architecture]({{ "/architecture" | url }}) for detailed design information.

Mnemosyne extends Microsoft's [LSP][] protocol for IDE integration
to support decorating program subtrees with tests, types, and
prose.  We call this extension "Argot LSP," or “Argot” for short.

<div class="figure">

![Argot]({{ "/argot.svg" | url }})

</div>

Inside Mnemosyne Argot allows automated techniques of program test
generation, program analysis, type and invariant inference, and
program synthesis and refactoring (we call these modules "[Muses]({{
"/muses" | url }})") to communicate [with each other][] as well as
with the developer's IDE.

[with each other]: {{ "/multi-muse-flows" | url }}

<div class="figure">

![Mnemosyne Architecture]({{ "/architecture.svg" | url }})

</div>

The result is a collaborative environment centered around the
developer where suggestions for new tests, types, and code edits are
continually made against the software code base.  The result is
improved developer productivity and software quality.

</div>
</div> <!-- end architecture -->

</div> <!-- end row -->

<div class="w3-row-padding">

<div class="w3-half w3-margin-bottom">
<div class="w3-container w3-card w3-light-gray intro-card">

## Muses

See [Muses]({{ "/muses" | url }}) for a complete list of the Modules
or *Muses* currently implemented and under development for Mnemosyne
with demo videos.  Some of these include:

<table>
<tr><th>Name</th><th>Description</th><th>Video</th></tr>

{% for muse in collections.front-muse %}

<tr>

{% if muse.data.url %}
  <td style="text-align:right;"><a href="{{ muse.data.url | url }}">{{ muse.data.name }}</a></td>
{% else %}
  <td style="text-align:right;">{{ muse.data.name }}</td>
{% endif %}
  <td>{{ muse.data.brief }}</td>
  <td style="text-align:center;"><a href="{{ muse.data.video | url }}"><i class="fa fa-play-circle"></i></a></td>
</tr>

{% endfor%}

</table>

See [multi-muse flows]({{ "/multi-muse-flows" | url }}) for examples
of multiple Muses collaborating to automate larger scale development
tasks.

</div>
</div> <!-- end muses -->

<div class="w3-half w3-margin-bottom">
<div class="w3-container w3-card w3-light-gray intro-card">

## Goals and Approach

See [About][] for general information about the
project and [Approach][] for our top-level
themes and approaches to human/computer collaboration.

The Mnemosyne project was started by [GrammaTech][] in collaboration
with leading program synthesis researchers: [Swarat Chaudhuri][] and
[Işıl Dillig][] from UT Austin, and [Armando Solar-Lezama][] from MIT.

Research in program synthesis and automated software engineering has
leapt forward in recent years thanks to advances in underlying machine
learning and constraint solving technologies.  We hope to bridge
research and practice by connecting research technologies to modern
programming languages and IDEs.  To ensure practical utility we focus
on the following themes:
- **Transparency**: All results of muses are displayed to the
  developer using familiar tests, types, and code edits.
- **Interactivity**: The developer's activities guide muses and
  curate their results.  Smaller more focused developer interactions
  are prioritized over coarse-grained large-scale synthesis.
- **Incrementality**: Intermediate results are communicated back to
  the developer and the software project.  Synthesis techniques that
  proceed function by function are prioritized when scalability is in
  question.

[About]: ({{ "/about" | url }})
[Approach]: ({{ "/approach" | url }})
[GrammaTech]: https://www.grammatech.com
[Işıl Dillig]: https://www.cs.utexas.edu/~isil
[Swarat Chaudhuri]: https://www.cs.utexas.edu/~swarat
[Armando Solar-Lezama]: https://people.csail.mit.edu/asolar

</div>
</div> <!-- end muses -->

</div> <!-- end row -->

## Copyright and Acknowledgments

Copyright (C) 2021 GrammaTech, Inc.

This material is based upon work supported by the US Air Force,
AFRL/RIKE and DARPA under Contract No. FA8750-20-C-0208.  Any
opinions, findings and conclusions or recommendations expressed in
this material are those of the author(s) and do not necessarily
reflect the views of the US Air Force, AFRL/RIKE or DARPA.

[LSP]: https://en.wikipedia.org/wiki/Language_Server_Protocol
[Eglot]: https://github.com/joaotavora/eglot
[LanguageClient-neovim]: https://github.com/autozimu/LanguageClient-neovim
[Plug]: https://github.com/junegunn/vim-plug
[vsix]: https://gitlab.com/GrammaTech/Mnemosyne/mnemosyne-vscode/-/releases
[vsix-howto]: https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix
[docker-compose]: https://docs.docker.com/compose/install/
[eglot-argot.el]: https://gitlab.com/GrammaTech/Mnemosyne/eglot-argot/-/blob/master/eglot-argot.el
[coc.nvim]: https://github.com/neoclide/coc.nvim
