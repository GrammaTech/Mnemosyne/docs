---
layout: default.liquid
title: About
---

About
=====

The Mnemosyne project is led by [GrammaTech][] in collaboration with
leading program synthesis researchers: [Swarat Chaudhuri][] and [Işıl
Dillig][] from UT Austin, and [Armando Solar-Lezama][] from MIT.  Our
work on Mnemosyne started in May 2020.  Mnemosyne is open-source
software and the underlying git repositories are available at
[https://gitlab.com/GrammaTech/Mnemosyne](https://gitlab.com/GrammaTech/Mnemosyne).

[GrammaTech]: https://www.grammatech.com
[Işıl Dillig]: https://www.cs.utexas.edu/~isil
[Swarat Chaudhuri]: https://www.cs.utexas.edu/~swarat
[Armando Solar-Lezama]: https://people.csail.mit.edu/asolar

## Goals

The goal for Mnemosyne is to provide an automated software development
environment which is *usable* and enables developers to build *better*
software *faster*.

Our efforts will be guided by the following measurable goals.  These
goals will be measured through our own use of Mnemosyne
(i.e. *dogfooding*).

1. **Usability**.  How easily is Mnemosyne used.  Is it difficult to
    identify, invoke, and then apply the results of the synthesis
    modules.  We will attempt to continually evaluate usability in our
    own development using Mnemosyne.  This is our most subjective
    metric.
2. **Quality**.  How good is the synthesized code returned by
    Mnemosyne.  More generally what is the quality of entire software
    projects developed using Mnemosyne.  Evaluation of this metric
    will leverage the *many* readily accessible tools for automated
    quality assessment of software projects; from linters and static
    analyzers to dynamic fuzzers.
3. **Scalability**.  There are two aspects to this question which we
   will attempt to measure independently.  First, how do our
   individual synthesis modules scale against task size.  From single
   expressions and statements, up to functions, and maybe eventually
   whole modules.  At least initially we may use lines of code (LOC)
   of synthesized code as a proxy for complexity.  Second is the
   question of how well Mnemosyne scales to the size of the overall
   project.  As Mnemosyne relies on the software developer to
   decompose the top-level requirements into pieces which are
   tractable to the available synthesis modules the system should
   productively contribute to *any* scale of software project (or just
   to portions of a software project).  However, it may still be the
   case that certain domains of software projects benefit more
   directly from the use of Mnemosyne for developer assistance.
4. **Automation**.  We will measure the overall impact of automation.
   We plan to augment our Argot-server to track the provenance of
   every character of code.  We can then review this information to
   measure to which degree specific synthesis modules and the
   developer contributed to the code base of a project over the course
   of development.

## Sponsored by

Mnemosyne development has been supported by the following projects:
- DARPA's [IDAS][] funded the initial development of Mnemosyne through August 2021.
- DARPA's [OPS-5G][] is building upon Mnemosyne in the automation of Specification/Code mapping and, eventually, code synthesis.

[IDAS]: https://www.darpa.mil/news-events/2019-07-02
[OPS-5G]: https://www.darpa.mil/program/open-programmable-secure-5g

## Copyright and Acknowledgments

Copyright (C) 2021 GrammaTech, Inc.

This material is based upon work supported by the US Air Force,
AFRL/RIKE and DARPA under Contract No. FA8750-20-C-0208.  Any
opinions, findings and conclusions or recommendations expressed in
this material are those of the author(s) and do not necessarily
reflect the views of the US Air Force, AFRL/RIKE or DARPA.
