---
layout: default.liquid
title: Muses
---

Muses
=====

The following Muses are under development to work with Mnemosyne.</br>
For information on developing new Muses see [Architecture#Muses]({{ "/architecture#muses" | url }}).</br>
For examples of multiple muses working together see [multi-muse flows]({{ "/multi-muse-flows" | url }}).

{% assign row = 0 %}
{% for muse in collections.muse %}
{% if muse.data.video %}

{% if row == 0 %}
<div class="w3-row-padding">
{% endif %}

<div class="w3-half w3-margin-bottom">
<div class="w3-container w3-card w3-light-gray muse-card" style="height:500px;">

### {{ muse.data.name }}

<div style="height=300px">
{% if muse.data.video %}
<center>
<video width="95%" style="max-height:300px" controls="controls" preload="metadata">
  <source src="{{ muse.data.video | url }}#t=0.01" type="video/mp4">
</video>
</center>
{% endif %}
</div>

{{ muse.data.desc }}

{% if muse.data.script %}- [Usage script]({{ muse.data.script | url }}).{% endif %}
{% if muse.data.url %}
- [More information]({{ muse.data.url | url }}).
{% elsif muse.data.slug %}
- [More information](https://gitlab.com/GrammaTech/Mnemosyne/Muses/{{ muse.data.slug }}).
{% endif %}

</div>
</div>

{% if row == 1 %}
</div>
{% assign row = 0 %}
{% endif %}

{% assign row = row | plus: 1 %}

{% endif %} <!-- Only Muses with Videos. -->
{% endfor %}
